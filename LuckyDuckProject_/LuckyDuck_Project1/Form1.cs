﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LuckyDuck_Project1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        
        //------------ Cerrar programa con X -----------------------------------------------------------------------
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            
        }

        //------------ Validacion Usuario --------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "root" && textBox2.Text == "root")
            {
                string parametro= textBox1.Text;
                this.Hide();
                Form3 Form3 = new Form3(parametro);
                Form3.Show();
            }
        }

        string user = "";
        string password = "";
        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = "User";
            textBox1.ForeColor = Color.Gray;

            textBox2.Text = "Password";
            textBox2.ForeColor = Color.Gray;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Equals("User"))
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Gray;
            }
            else
            {
                textBox1.ForeColor = Color.Black;
            }
        }
        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Equals("Password"))
            {
                textBox2.Text = "";
                textBox2.ForeColor = Color.Gray;
            }
            else
            {
                textBox2.ForeColor = Color.Black;
            }

        }
        private void textBox1_Leave(object sender, EventArgs e)
        {
            user = textBox1.Text;
            if (user.Equals("User"))
            {
                textBox1.Text = "User";
                textBox1.ForeColor = Color.Gray;
            }
            else
            {
                if (user.Equals(""))
                {
                    textBox1.Text = "User";
                    textBox1.ForeColor = Color.Gray;
                }
                else
                {
                    textBox1.Text = user;
                    textBox1.ForeColor = Color.Black;
                }
            }
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            password = textBox2.Text;
            if (password.Equals("Password"))
            {
                textBox2.Text = "Password";
                textBox2.ForeColor = Color.Gray;
            }
            else
            {
                if (password.Equals(""))
                {
                    textBox2.Text = "Password";
                    textBox2.PasswordChar = '\0';
                    textBox2.ForeColor = Color.Gray;
                }
                else
                {
                    textBox2.PasswordChar = '●';
                    textBox2.Text = password;
                    textBox2.ForeColor = Color.Black;
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
