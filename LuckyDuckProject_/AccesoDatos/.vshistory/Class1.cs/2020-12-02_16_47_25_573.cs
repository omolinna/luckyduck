﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Configuration;

namespace AccesoDatos
{
    public class Class1
    {
        string cnx;
        private SqlConnection conn;
        DataSet dts;
        string query;

        private void Connectar()
        {
            cnx = "Data Source=LAPTOP-52SBLPRR\\SQLEXPRESS;Initial Catalog=SecureCore;Integrated Security=True";
            conn = new SqlConnection(cnx);
            conn.Open();
        }

        /*public void encriptar()
        {
            Connectar();
            
            Configuration conf = ConfigurationManager.OpenExeConfiguration("App.exe");
            ConnectionStringsSection section = conf.GetSection("connectionStrings")
            as ConnectionStringsSection;

            if (!section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            conf.Save();
        }*/

        public DataTable ConfigurarConexio()
        {
            Connectar();

            //conn = new SqlConnection(cnx);

            SqlDataAdapter adapter;
            query = "select * from users";
            DataSet dts = new DataSet();
            adapter = new SqlDataAdapter(query, conn);
            adapter.Fill(dts, "users");

            conn.Close();

            return dts.Tables[0];
        }

        public void Actualizar()
        {
            Connectar();

            SqlDataAdapter adapter;
            adapter = new SqlDataAdapter(query, conn);

            SqlCommandBuilder cmdBuilder;
            cmdBuilder = new SqlCommandBuilder(adapter);
            adapter.Update(dts.Tables[0]);

            conn.Close();
        }

        public DataTable PortarPerTaula(string taula)
        {
            Connectar();
            
            dts = new DataSet();
            SqlDataAdapter adapter;
            query = "select * from " + taula;
            
            adapter = new SqlDataAdapter(query, conn);
            adapter.Fill(dts, taula);

            conn.Close();

            return dts.Tables[taula];
        }

        public string XML()
        {
            //ConfigurarConexio();
            string XMLDataset = dts.GetXml();

            return XMLDataset;
        }

        public DataSet PortarPerConsulta(string consulta) 
        { 
            Connectar();

            SqlDataAdapter adapter;
            adapter = new SqlDataAdapter(consulta, conn);
            DataSet dts = new DataSet();
            adapter.Fill(dts);            
            conn.Close();

            return dts;
        }


        public DataSet PortarPerConsulta(string consulta, string nombre_tabla)
        {
            query = consulta;
            Connectar();

            SqlDataAdapter adapter;
            adapter = new SqlDataAdapter(query, conn);
            DataSet dts = new DataSet();
            adapter.Fill(dts,nombre_tabla);
            conn.Close();

            return dts;
        }
        /* public DataSet PortarPerConsulta(string consulta, string nombre_tabla, string nom) 
         {
             query = consulta;
             Connectar();

             SqlDataAdapter adapter;
             adapter = new SqlDataAdapter(query, conn);
             DataSet dts = new DataSet();
             adapter.Fill(dts, nombre_tabla, nom);
             conn.Close();

             return dts;

         }*/
        public DataTable añadir_fila()
        {
            Connectar();

            SqlDataAdapter adapter;
            query = "INSERT INTO [dbo].[Users] ([CodeUser]) VALUES ('')";
            dts = new DataSet();
            adapter = new SqlDataAdapter(query, conn);
            adapter.Fill(dts, "users");

            conn.Close();

            return dts.Tables["users"];
        }
    }

}
